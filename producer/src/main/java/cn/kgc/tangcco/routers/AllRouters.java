package cn.kgc.tangcco.routers;

import cn.kgc.tangcco.handler.ClazzHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/22 13:55
 */
@Configuration
public class AllRouters {
    @Bean
    public RouterFunction<ServerResponse> clazzRouter(ClazzHandler clazzHandler) {
        return RouterFunctions.nest(
                RequestPredicates.path("/funclazz"),
                RouterFunctions.route(RequestPredicates.GET("/findAll"),clazzHandler::findAll)
        );
    }
}
