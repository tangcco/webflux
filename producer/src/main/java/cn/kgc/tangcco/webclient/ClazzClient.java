package cn.kgc.tangcco.webclient;

import cn.kgc.tangcco.pojo.Clazz;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/22 14:19
 */
@RestController
@RequestMapping(value = "/clazzClient")
public class ClazzClient {
    // 创建 WebClient 对象
    private static WebClient webClient = WebClient.builder()
            .baseUrl("http://localhost/clazz")
            .build();

    @GetMapping(value = "/findById/{id}")
    public Mono<Clazz> findById(@PathVariable("id") Integer id) {
        // 订阅（异步处理结果）
        return webClient
                .get() // GET 请求
                .uri("/findById/{id}", id)  // 请求路径
                .retrieve() // 获取响应体
                .bodyToMono(Clazz.class); //响应数据类型转换
    }

    @GetMapping(value = "/findAll")
    public Flux<Clazz> findAll() {
        return webClient
                .get() // GET 请求
                .uri("/findAllWithStream")  // 请求路径
                .retrieve() // 获取响应体
                .bodyToFlux(Clazz.class); //响应数据类型转换
    }

    @PutMapping(value = "/save")
    public Mono<Clazz> save(Clazz clazz) {
        return webClient
                .put() // put 请求
                .uri("/save")  // 请求路径
                .body(clazz, Clazz.class)
                .header(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .retrieve() // 获取响应体
                .bodyToMono(Clazz.class); //响应数据类型转换
    }
}
