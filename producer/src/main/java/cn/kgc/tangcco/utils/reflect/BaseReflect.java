package cn.kgc.tangcco.utils.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * @author 李昊哲
 * @version 1.0 父类向子类转换传值
 * @date 2020/5/22 下午1:10
 */
public abstract class BaseReflect {
    /**
     * @param <T>    数据类型
     * @param father 父类对象
     * @param child  子类对象
     * @throws Exception Exception
     */
    public static <T> T fatherToChildWithFiled(T father, T child) throws Exception {
//        if (child.getClass().getSuperclass() != father.getClass()) {
//            throw new Exception("child 不是 father 的子类");
//        }
        Class<?> fatherClass = father.getClass();
        Field[] declaredFields = fatherClass.getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            if (field.get(father) != null) {
                // 判断该属性是否为常量
                boolean isFinal = Modifier.isFinal(field.getModifiers());
                if (isFinal && "serialVersionUID".equals(field.getName())) {
                    // 如果是常量且属性名为serialVersionUID跳过本次循环
                    continue;
                }
                field.set(child, field.get(father));
            }
        }
        return child;
    }
    public static <T> T convert4jpa(T param, T db) throws Exception {
//        if (child.getClass().getSuperclass() != father.getClass()) {
//            throw new Exception("child 不是 father 的子类");
//        }
        Class<?> paramClass = param.getClass();
        Field[] declaredFields = paramClass.getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            // 判断该属性是否为常量
            boolean isFinal = Modifier.isFinal(field.getModifiers());
            if (isFinal && "serialVersionUID".equals(field.getName())) {
                // 如果是常量且属性名为serialVersionUID跳过本次循环
                continue;
            }
            if (field.get(param) == null) {
                field.set(param, field.get(db));
            }
        }
        return param;
    }
}
