package cn.kgc.tangcco.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author 李昊哲
 * @Description
 * @create 2020/10/30 9:23
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "student")
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler", "fieldHandler"})
public class Student implements Serializable {
    private static final long serialVersionUID = -674409358640403148L;
    /**
     * 学生编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * 学生唯一标志
     */
    @Column(name = "uuid")
    private String uuid;

    /**
     * 学生曾用名
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * 学生现用名
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * 学生身份证
     */
    @Column(name = "id_card")
    private String idCard;

    /**
     * 学生手机号
     */
    @Column(name = "mobile")
    private String mobile;

    /**
     * 密码
     */
    @Column(name = "auth_text")
    private String authText;

    /**
     * 学生所在班级编号
     */
    @Column(name = "cid")
    private Integer cid;

    /**
     * 学生所在班级信息
     */
    @ManyToOne(targetEntity = Clazz.class, cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "cid", referencedColumnName = "id", insertable = false, updatable = false, nullable = true)
    private Clazz clazz;

    public Student(String uuid, String firstName, String lastName, String idCard, String mobile, String authText) {
        this.uuid = uuid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.idCard = idCard;
        this.mobile = mobile;
        this.authText = authText;
    }

    public Student(String uuid, String firstName, String lastName, String idCard, String mobile, String authText, Integer cid) {
        this.uuid = uuid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.idCard = idCard;
        this.mobile = mobile;
        this.authText = authText;
        this.cid = cid;
    }

    public Student(String uuid, String firstName, String lastName, String idCard, String mobile, String authText, Clazz clazz) {
        this.uuid = uuid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.idCard = idCard;
        this.mobile = mobile;
        this.authText = authText;
        this.clazz = clazz;
    }

    public Student(String uuid, String firstName, String lastName, String idCard, String mobile, String authText, Integer cid, Clazz clazz) {
        this.uuid = uuid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.idCard = idCard;
        this.mobile = mobile;
        this.authText = authText;
        this.cid = cid;
        this.clazz = clazz;
    }
}
