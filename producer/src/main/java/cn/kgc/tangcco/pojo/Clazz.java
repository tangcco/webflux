package cn.kgc.tangcco.pojo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author 李昊哲
 * @Description
 * @create 2020/10/30 9:19
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "clazz")
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler", "fieldHandler"})
public class Clazz implements Serializable {
    private static final long serialVersionUID = -1546984557874640144L;
    /**
     * 班级编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    /**
     * 班级名称
     */
    @Column(name = "name")
    private String name;

    @JsonBackReference
    // @JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
    @OneToMany(targetEntity = Student.class, mappedBy = "clazz", cascade = {CascadeType.ALL}, orphanRemoval = false, fetch = FetchType.LAZY)
    public List<Student> students;

    public Clazz(String name) {
        this.name = name;
    }

    public Clazz(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Clazz(String name, List<Student> students) {
        this.name = name;
        this.students = students;
    }

}
