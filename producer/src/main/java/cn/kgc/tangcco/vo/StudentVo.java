package cn.kgc.tangcco.vo;

import cn.kgc.tangcco.pojo.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class StudentVo extends Student {
    /**
     * 性别 1代表男性 0代表女性
     */
    private Integer gender;
    /**
     * 总人数
     */
    private Integer total;
}
