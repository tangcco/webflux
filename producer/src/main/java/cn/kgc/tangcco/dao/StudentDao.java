package cn.kgc.tangcco.dao;

import cn.kgc.tangcco.pojo.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/22 9:17
 */
@Repository
public interface StudentDao extends JpaRepository<Student, Integer>, JpaSpecificationExecutor<Student> {
}
