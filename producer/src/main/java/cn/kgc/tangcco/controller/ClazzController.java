package cn.kgc.tangcco.controller;

import cn.kgc.tangcco.dao.ClazzDao;
import cn.kgc.tangcco.pojo.Clazz;
import cn.kgc.tangcco.service.ClazzService;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/22 9:47
 */
@RestController
@RequestMapping(value = "/clazz")
public class ClazzController {

    private final ClazzService clazzService;

    public ClazzController(ClazzService clazzService) {
        this.clazzService = clazzService;
    }

    @PutMapping(value = "/save")
    public Mono<Clazz> save(Clazz clazz) {
        return Mono.justOrEmpty(clazzService.save(clazz));
    }

    @GetMapping(value = "/findById/{id}")
    public Mono<Clazz> findById(@PathVariable("id") Integer id) {
        return Mono.justOrEmpty(clazzService.findById(id));
    }

    @GetMapping(value = "/findAll")
    public Flux<List<Clazz>> findAll() {
        return Flux.just(clazzService.findAll());
    }

    @GetMapping(value = "/findAllWithStream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Clazz> findAllWithStream() {
        List<Clazz> list = clazzService.findAll();
        return Flux.fromStream(list.stream().map(clazz -> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return clazz;
        }));
    }
}
