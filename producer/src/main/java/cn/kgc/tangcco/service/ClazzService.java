package cn.kgc.tangcco.service;

import cn.kgc.tangcco.pojo.Clazz;

import java.util.List;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/22 10:05
 */
public interface ClazzService {

    public Clazz save(Clazz clazz);

    public Clazz findById(Integer id);

    public List<Clazz> findAll();
}
