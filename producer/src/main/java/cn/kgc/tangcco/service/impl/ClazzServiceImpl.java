package cn.kgc.tangcco.service.impl;

import cn.kgc.tangcco.dao.ClazzDao;
import cn.kgc.tangcco.pojo.Clazz;
import cn.kgc.tangcco.service.ClazzService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/22 12:13
 */
@Service
public class ClazzServiceImpl implements ClazzService {
    private final ClazzDao clazzDao;

    public ClazzServiceImpl(ClazzDao clazzDao) {
        this.clazzDao = clazzDao;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class}, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false)
    public Clazz save(Clazz clazz) {
        return clazzDao.saveAndFlush(clazz);
    }


    @Override
    public Clazz findById(Integer id) {
        Optional<Clazz> optional = clazzDao.findById(id);
        if (!optional.isEmpty()) {
            return optional.get();
        }
        return null;
    }

    @Override
    public List<Clazz> findAll() {
        return clazzDao.findAll();
    }
}
