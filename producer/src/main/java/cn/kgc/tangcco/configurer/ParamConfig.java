package cn.kgc.tangcco.configurer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 李昊哲
 * @Description 获取springboot核心配置文件中的自定义参数
 * @create 2020/12/26 13:59
 */
@Configuration
@ConfigurationProperties()
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ParamConfig {
    /**
     * aes密钥
     */
    @Value("${key.aes}")
    private String aes;
    /**
     * des密钥
     */
    @Value("${key.des}")
    private String des;

    /**
     * 跨域拦截地址
     */
    @Value("${cors.pathPattern}")
    private String pathPattern;

    /**
     * 允许跨域访问资源的访问者的url 注意：若参数值为*时 accessControlAllowCredentials 参数必须为false
     */
    @Value("${cors.response.header.accessControlAllowOrigin}")
    private String accessControlAllowOrigin;

    /**
     * 允许跨域的requestMethod
     */
    @Value("${cors.response.header.accessControlAllowMethods}")
    private String accessControlAllowMethods;

    /**
     * 预检请求间隔
     * 0 表示每次异步请求都发起预检请求，也就是说，发送两次请求。
     * 3600 表示隔60分钟才发起预检请求。也就是说，发送两次请求
     */
    @Value("${cors.response.header.accessControlAllowMaxAge}")
    private String accessControlAllowMaxAge;

    /**
     * 接受的 requestHeader包含
     */
    @Value("${cors.response.header.accessControlAllowHeaders}")
    private String accessControlAllowHeaders;

    /**
     * 客户端是否携带cookie
     * false 为不允许
     * true 为运行 注意：若参数值true时 accessControlAllowOrigin的参数值不能为* 必须指定允许跨域访问资源的访问者的url
     */
    @Value("${cors.response.header.accessControlAllowCredentials}")
    private String accessControlAllowCredentials;
}
