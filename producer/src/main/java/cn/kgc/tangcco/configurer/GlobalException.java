package cn.kgc.tangcco.configurer;

import cn.kgc.tangcco.model.ResponseText;
import cn.kgc.tangcco.model.ResultCode;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author 李昊哲
 * @Description 全局异常处理 返回 json
 * @create 2021/1/15 12:12
 */
@RestControllerAdvice
public class GlobalException {

    @ExceptionHandler(value = {Exception.class, RuntimeException.class})
    public ResponseText defaultErrorHandler(Exception e) throws Exception {
        e.printStackTrace();
        return new ResponseText(ResultCode.EXCEPTION.getCode(),e.getMessage(),null);
    }
}
