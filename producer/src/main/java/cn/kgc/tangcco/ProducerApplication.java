package cn.kgc.tangcco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }

    @Bean
    public TomcatServletWebServerFactory mbeddedServletContainerFactory() {
        TomcatServletWebServerFactory tomcatEmbeddedServletContainerFactory = new TomcatServletWebServerFactory();

        tomcatEmbeddedServletContainerFactory.addConnectorCustomizers(connector -> {
            connector.setMaxParameterCount(Integer.MAX_VALUE);
        });

        return tomcatEmbeddedServletContainerFactory;
    }

}
