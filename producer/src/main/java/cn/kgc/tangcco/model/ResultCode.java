package cn.kgc.tangcco.model;

/**
 * @author 李昊哲
 * @Description 返回值状态码枚举
 * @date 2020/12/10 下午2:01
 */
public enum ResultCode {
    /**
     * 操作成功 请求成功
     */
    SUCCESS("200", "操作成功", "请求成功"),

    /**
     * 操作失败 执行失败
     */
    FAILED("10086", "执行失败", "执行失败"),

    /**
     * 参数错误 参数为空或格式不正确
     */
    PARAM_ERROR("10001", "参数错误", "参数为空或格式不正确"),

    /**
     * 登录失败
     */
    LOGIN_FAILED("10002", "登录失败", "登录失败"),


    /**
     * appKey异常 appKey被冻结
     */
    APPKEY_ERROR("10005", "appKey异常", "appKey被冻结"),

    /**
     * 验证码失效 redis中key失效
     */
    TIMEOUT("10006", "验证码失效，请重新发送", "redis中key失效"),

    /**
     * 短信一发送，单位时间内，不会重新发送
     */
    NO_TIMEOUT("10007", "短信已发送，请等待", "短信一发送，单位时间内，不会重新发送"),

    /**
     * 短信一发送，单位时间内，不会重新发送
     */
    CODE_ERROR("10008", "验证码错误，请重新输入", "客户端获取的验证码与redis中存储的验证码不一致"),

    /**
     * 短信一发送，单位时间内，不会重新发送
     */
    NO_LOGIN("10009", "未登录状态", "未登录状态"),

    /**
     * 未知系统异常
     */
    EXCEPTION("10010", "未知系统异常", "未知系统异常"),

    /**
     * appKey不存在 确认appKey是否正确
     */
    APPKEY_NOTHINGNESS("10017", "appKey不存在", "确认appKey是否正确"),

    /**
     * appkey和appSecret不匹配
     */
    APPKEY_MISMATCHING("10030", "appkey和appSecret不匹配", "appkey和appSecret不匹配"),

    /**
     * 数据异常 接口调用异常
     */
    DATA_ERROR("49999", "数据异常", "接口调用异常"),

    /**
     * 数据异常 接口调用异常
     */
    DATA_EMPTY("50000", "未查询到数据", "未查询到数据"),

    /**
     * 手机号已经存在
     */
    MOBILE_EXISTS("50001", "手机号已经存在", "手机号已经存在"),

    /**
     * 手机号不存在
     */
    MOBILE_NOT_EXISTS("50002", "手机号不存在", "手机号不存在");

    /**
     * 状态码
     */
    private String code;
    /**
     * 状态码含义
     */
    private String msg;
    /**
     * 状态码含义描述
     */
    private String desc;

    ResultCode(String code, String msg, String desc) {
        this.code = code;
        this.msg = msg;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public String getDesc() {
        return desc;
    }
}
