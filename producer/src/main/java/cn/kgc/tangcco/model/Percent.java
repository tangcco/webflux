package cn.kgc.tangcco.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author 李昊哲
 * @version 1.0
 * @date 2020/7/6 上午11:24
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Accessors(chain = true)
public class Percent {
    private String name;
    private Integer value;
}
